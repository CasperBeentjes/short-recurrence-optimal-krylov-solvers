# README #

### What is this repository for? ###

* Matlab implementation of some short recurrence optimal Krylov solvers (SUMR and PGMRES). See [essay](https://cbeentjes.github.io/notes/2013-Faber-Manteuffel-Review) for a more detailed exposure.
* Current solvers

1. SUMR (with and without explicit normalisation)
2. PGMRES (own implementation and fastgmres by Reichel & Beckermann)

### How do I use the Matlab scripts? ###

* TestSuite: contains a test suite of several matrix problems to retrieve results in [essay](https://cbeentjes.github.io/notes/2013-Faber-Manteuffel-Review)
* Solvers: contains the aforementioned solvers + LMR and GMRES for comparison

### Who do I talk to? ###

* Casper Beentjes [beentjes@maths.ox.ac.uk](mailto:beentjes@maths.ox.ac.uk)