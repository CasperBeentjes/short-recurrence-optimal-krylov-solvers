function [x,res,V] = PGMRES( A, b, F1, G1, maxit, tol, x0, vector )
%PGMRES Progressive Generalized Minimal Residual Method
% Use for solving system of linear equations A*x = b
% for x. For PGMRES it is supposed that A - F1*G1' is Hermitian.
%
% input     A         real or complex unitary matrix   
%           b         real or complex right hand side vector
%           F1,G1     F1*G1' real or complex perturbation of A (optional)
%           maxit     integer maximum number of iterations (optional)
%           tol       real residual error tolerance (optional)
%           x0        real or complex initial guess vector (optional)
% 
%  output   x         real or complex solution vector
%           res       vector of residual norms
%           V         Krylov subspace vectors

% Version 29-1-2014 Casper Beentjes

% Assign default values to unspecified parameters
if (nargin < 2)
    error('Not enough input given')
end
n = size(A,1);
if (nargin < 3) || isempty(F1)
    F1 = zeros(n,1);
end
if (nargin < 4) || isempty(G1)
    G1 = zeros(n,1);
end 
if (nargin < 5) || isempty(maxit)
    maxit = 50;
end
if (nargin < 6) || isempty(tol)
    tol = 1e-6;
end
if (nargin < 7) || isempty(x0)
    x0 = zeros(n,1);
end
if (nargin < 8) || isempty(vector)
    vector = 0;
end
V = [];

%Initialize F and by C also G = F*C
F = [F1,G1];
s2 = size(F1,2);
I = speye(s2);
D = [0,-1;1,0];
C = kron(D,I);

% Set up
residual = b - A*x0;
gamma = norm(residual);
v = residual/gamma;
z = x0/gamma;
c_old = 1;
res = gamma;
if vector == 1
    V = v;
end

% j = 1
T = spalloc(maxit+1,maxit+1,3*(maxit+1)); % initialize matrix T
% Arnoldi step
Fhat = v'*F;
Ghat = Fhat*C;
Ftilde = v*Fhat;
v0 = A*v - Ftilde*Ghat';
T(1,1) = v'*v0;
v0 = v0 - T(1,1)*v;
T(2,1) = norm(v0);
v_old = v;
v = v0/T(2,1);
if vector == 1
    V = [V,v];
end

% Minres step
pstar = Fhat;
W = z*Fhat;
tau = T(1,1)';
    % Givens rotation angles
    c = tau/sqrt(tau*tau'+T(2,1)^2);
    s = T(2,1)/sqrt(tau*tau'+T(2,1)^2);
gamma = -s*gamma;
z_old = z;
z = -(v_old+tau*z)/T(2,1);
x = s^2*x0+gamma*c'*z;
res = [res, abs(gamma)];

% j > 2
for j = 2:maxit
    % Arnoldi step
    Fhat = v'*F;
    Ghat = Fhat*C;
    Ftilde = Ftilde + v*Fhat;
    v0 = A*v - Ftilde*Ghat';
    T(j-1,j)=v_old'*v0;
    v0 = v0 - T(j-1,j)*v_old;
    T(j,j) = v'*v0;
    v0 = v0 - T(j,j)*v;
    T(j+1,j) = norm(v0);
    v_old = v;
    v = v0/T(j+1,j);
    if vector == 1
        V = [V,v];
    end
    
    % Minres step
    pstar = -s*pstar+c*Fhat;
    W = W + z*Fhat;
    w = z;
    z = -1/T(j+1,j)*(v_old+T(j,j)*z+T(j-1,j)*z_old+W*Ghat');
    z_old = w;        
    tau = c*T(j,j) - s*c_old*T(j-1,j) + pstar*Ghat';
    % Givens rotation angles
        c_old = c;
        c = tau/sqrt(tau*tau'+T(j+1,j)^2);
        s = T(j+1,j)/sqrt(tau*tau'+T(j+1,j)^2);
    gamma = - s*gamma;
    res = [res, abs(gamma)];
    x = s^2*x + gamma*c'*z;
    
    if res(end) < tol
        display('PGMRES converged')
        break
    end
end

end

