function [x,res,V] = SUMR( U, b, shift, scaling, maxit, tol, x0, vector )
%SUMR Shifted Unitary Minimal Residual Method
% Use for solving system of linear equations A*x = b
% for x. Where A = shift*I + scaling*U with U a square
% unitary matrix.
%
% input     U         real or complex unitary matrix   
%           b         real or complex right hand side vector
%           shift     real or complex shift of unitary matrix (optional)
%           scaling   real or complex scaling of unitary matrix (optional)
%           maxit     integer maximum number of iterations (optional)
%           tol       real residual error tolerance (optional)
%           x0        real or complex initial guess vector (optional)
%           vector    integer 0 = no basis for Krylov space is saved
%                           1 = basis for Krylov space is saved in V
% 
%  output   x         real or complex solution vector
%           res       vector of residual norms
%           V         Krylov subspace basis vectors

% Version 29-1-2014 Casper Beentjes

% Assign default values to unspecified parameters
if (nargin < 2)
    error('Not enough input given')
end
if (nargin < 3) || isempty(shift)
    shift = 0;
end
if (nargin < 4) || isempty(scaling)
    scaling = 1;
end
if (nargin < 5) || isempty(maxit)
    maxit = 20;
end
if (nargin < 6) || isempty(tol)
    tol = 1e-6;
end
if (nargin < 7) || isempty(x0)
    n = size(U,2);
    x0 = zeros(n,1);
end
if (nargin < 8) || isempty(vector)
    vector = 0;
end
V = [];

% Set up parameters
rho = scaling;
zeta = shift;
% Set up initial values
x = x0;
residual = b - rho*U*x0 - zeta*x0;
res = norm(residual);
phibar = 1/res;
taubar = res/rho;
w_old =  zeros(n,1); p = w_old; v_old = w_old;
phi = 0; s = 0; lambda = 0; 
r_old = 1; c = 1;
v = residual/res;
d = res;
vbar = v;
if vector == 1
   V = v; 
end

for i = 1:maxit
    % Isometric Arnoldi process
    u = U*v;
    gamma = -vbar'*u; % angle Givens matrix
    sigma = sqrt((1-abs(gamma))*(1+abs(gamma))); % angle Givens matrix
    % QR factorization of Hessenberg decomposition of U
    alpha = -gamma*d;
    q = alpha*phi + s*zeta/rho;
    rbar = alpha*phibar + c'*zeta/rho;
    z = sqrt(rbar*rbar'+sigma*sigma');
    c = (rbar/z)'; % angle Givens matrix
    s = -sigma/z; % angle Givens matrix
    r = -c*rbar + s*sigma;
    d = d*sigma;
    phi = -c*phibar + s*gamma'/d;
    phibar = s*phibar + c'*gamma'/d;
    % Minres update of x
    tau = -c*taubar;
    taubar = s*taubar;
    eta = tau/r;
    kappa = q/r_old;
    w = alpha*p - kappa*(w_old-v_old);
    p = p - lambda*(w_old-v_old);
    x = x - eta*(w-v);
    lambda = phi/r;
    % Isometric Arnoldi process
    v_old = v;
    v = (1/sigma)*(u + gamma*vbar);
    v = v/norm(v); % explicit normalization
    vbar = sigma*vbar + gamma'*v;
    vbar = vbar/norm(vbar); % explicit normalization
    res = [res,abs(taubar)];
    if abs(taubar) < tol
        display('SUMR converged')
        break
    end
    % Store Krylov subspace basis
    if vector == 1
       V = [V,v]; 
    end
    % update vectors
    r_old = r;
    w_old = w;
end

end

