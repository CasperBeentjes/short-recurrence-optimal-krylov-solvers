function [x,hist,V,t]=gmresSleijpen(A,b,x0,tol,kmax,flag,flag0)

  %%% set defaults
  if nargin<7, flag0=1; end
  if nargin<6, flag='gmres'; end
  if nargin<5, kmax=100; end
  if nargin<4, tol=1.0e-8; end
  if nargin<3, x0=0*b; end
  
  %%% the version that allocates memory space first:
  if ~flag0,
    [x,hist,t]=gmres0(A,b,x0,tol,kmax,flag);
    return
  end
  
  

  FOM=strcmp(lower(flag),'fom');

  t0=clock; 
  %%%
  [n,m]=size(b); 

  x=x0;
  r=b-A*x;
  rho0=norm(r); rho=rho0; hist=rho;
  %%% scale tol for relative residual reduction
  tol=rho*tol;
  
  H=zeros(1,0);
  V=[r/rho]; gamma=1; nu=1;
  for k=1:kmax
     if rho<tol, display('GMRES converged'),
         break, end
     [V,H]=ArnStep(A,V,H,-1);
     %%% compute the nul vector of H'
     gk=(gamma*H(1:k,k))/H(k+1,k); gamma=[gamma,-gk];
     %%% Compute the residual norm
     if FOM
        rho=rho0/abs(gk);
     else
        nu=nu+gk'*gk; rho=rho0/sqrt(nu);
     end
     hist=[hist,rho];
  end
  %%% solve the low dimensional system
  k=size(H,2); e=zeros(k,1); e(1,1)=1;
  if FOM
     y=H(1:k,:)\e;
  else
     e=[e;0]; y=H\e;
  end
  %%% compute the approximate solution 
  x=V(:,1:k)*(rho0*y);

  t=etime(clock,t0);

return
