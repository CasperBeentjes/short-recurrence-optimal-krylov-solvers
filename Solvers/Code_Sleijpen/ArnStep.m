function [V,H]=ArnStep(A,V,H,kappa)

   if nargin<4, kappa=0.2; end

   [n,k]=size(V);
   v=A*V(:,k);
   [v,h]=Orth(V,v,kappa);
   V=[V,v]; 
   H=[[H;zeros(1,k-1)],h];

return
