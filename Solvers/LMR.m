function [ x, res] = LMR( A, b, alpha, maxit, tol, x0)
%LMR: Local Minimal Residual method
% Both Richardson (alpha given) and LMR method (alpha free)

if (nargin < 2)
    error('Not enough input given')
end
[n,~] = size(A);
% If alpha is given use Richardson iteration
% Else use local minimal residual alpha
if (nargin < 3) || isempty(alpha)
    local = true;
else
    local = false;
end
if (nargin < 4) || isempty(maxit)
    maxit = 20;
end
if (nargin < 5) || isempty(tol)
    tol = 1e-6;
end
if (nargin < 6) || isempty(x0)
    x0 = zeros(n,1);
end

r = b-A*x0;
res = norm(r);
x = x0;

for i = 1:maxit
   u = r;
   c = A*u;
   if local == true
      alpha = c'*r/(c'*c); 
   end
   r = r - alpha*c;
   x = x + alpha*u;
   R = norm(r);
   res = [res, R];
   if R < tol
      display('LMR converged')
      break
   end
end

end