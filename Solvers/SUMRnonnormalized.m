function [ x, res, V ] = SUMR( U, b, shift, scaling, maxit, tol, x0, vector )
% SUMR: Shifted Unitary Minimal Residual method
% Use for shifted Unitary matrices


% Assign default values to unspecified parameters
if (nargin < 2)
    error('Not enough input given')
end
[m,n] = size(U);
if (nargin < 3) || isempty(shift)
    shift = 0;
end
if (nargin < 4) || isempty(scaling)
    scaling = 1;
end
if (nargin < 5) || isempty(maxit)
    maxit = 20;
end
if (nargin < 6) || isempty(tol)
    tol = 1e-6;
end
if (nargin < 7) || isempty(x0)
    x0 = zeros(n,1);
end
if (nargin < 8) || isempty(vector)
    vector = 0;
end

% Set up
rho = scaling;
zeta = shift;
x = x0;
residual = b - rho*U*x0 - zeta*x0;
d = norm(residual);
phibar = 1/d;
taubar = d/rho;
wmin =  zeros(n,1); p = wmin; vmin = wmin;
phi = 0; 
s = 0; 
lambda = 0; 
rmin = 1; 
c = 1;
v = residual/d;
vbar = v;
res = d;
if vector == 1
   V = v; 
end

for i = 1:maxit
    u = U*v;
    gamma = -vbar'*u;
    sigma = sqrt((1-abs(gamma))*(1+abs(gamma)));
    alpha = -gamma*d;
    q = alpha*phi + s*zeta/rho;
    % 
    rbar = alpha*phibar + c'*zeta/rho;
    %
    z = sqrt(rbar*rbar'+sigma*sigma');
    c = (rbar/z)';
    s = -sigma/z;
    r = -c*rbar + s*sigma;
    tau = -c*taubar;
    taubar = s*taubar;
    eta = tau/r;
    kappa = q/rmin;
    w = alpha*p - kappa*(wmin-vmin);
    p = p - lambda*(wmin-vmin);
    w-v;
    x = x - eta*(w-v);
    %
    d = d*sigma;
    %
    phi = -c*phibar + s*gamma'/d;
    lambda = phi/r;
    phibar = s*phibar + c'*gamma'/d;
    vmin = v;
    v = (1/sigma)*(u + gamma*vbar);
%    v = v/norm(v);
    vbar = sigma*vbar + gamma'*v;
%    vbar = vbar/norm(vbar);
    res = [res,abs(taubar)];
    if abs(taubar) < tol
        display('SUMR converged')
        break
    end
    if vector == 1
       V = [V,v]; 
    end
    rmin = r;
    wmin = w;
end



   


end

