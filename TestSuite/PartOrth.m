function [ PO ] = PartOrth(V,L,name)
% Compute partial orthogonality Arnoldi vectors
% orthonormality between the last m Arnoldi vectors
% for R<=m<=L
R = L-4;
PL = cellstr(['k- ';'k--';'k-.';'k: ';'kx ']);
q = size(V,2);
PO = zeros(q,L);
for i = R:L
    for k = 1:q
        if k < i+1
        PO(k,i)=norm(speye(k,k)-V(:,1:k)'*V(:,1:k));          
        else
        PO(k,i)=norm(speye(i+1,i+1)-V(:,k-i:k)'*V(:,k-i:k));    
        end
    end
end

figure
legendnames = cell(1,L-R+1);
for i = R:L
    semilogy(1:q,PO(:,i), char(PL(i-R+1)),'linewidth',2);
    legendnames{i-R+1} = ['m = ' num2str(i)];
    hold on
end
title(['Partial Orthogonality Arnoldi vectors ', name])
xlabel('MV');
ylabel('||I_m-V(:,k-m:m)^*V(:,k-m:m)||_2');
legend(legendnames,'Location','southeast');
hold off
end

