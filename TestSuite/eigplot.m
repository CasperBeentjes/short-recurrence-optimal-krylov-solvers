function eigplot(A)
    D = eig(A);
    figure
    plot(real(D),imag(D),'xk')
    xlim([xmin,xmax]); ylim([ymin,ymax])
    %axis equal
    xlabel('Re(\lambda)')
    ylabel('Im(\lambda)')
end
