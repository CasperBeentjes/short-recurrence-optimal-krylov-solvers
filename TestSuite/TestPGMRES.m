%% Test suite for PGMRES
% For further description of examples see 
% http://people.maths.ox.ac.uk/beentjes/Essays/
clear all; close all
addpath('../Solvers/') % load solvers
addpath('../Solvers/Code_Sleijpen/') % load solvers by G. Sleijpen
lw = 'linewidth';

%% Examples
tol = 1e-10; % tolerance
n = 200; % Matrix size
M = input('Enter example number (1,2,3): ');
%====================================================
        % Construct Hermitian part
        % B = rand(n,n);
        % H = 1/2*(B+B'); % random Hermitian matrix
        %--
        % H = diag(10.+rand(n,1));
        %--
switch M
    case 1
        % Example 6.1 with ||F*G|| small
        % Clusters of eigenvalues, variation on example 5.2 SUMR
        m = 10; % number of clusters
        R = -10; % right side interval
        L = 5; % left side interval
        e = L+(R-L)*rand(m,1);
        epsilon = 0.1; % spread of cluster
        nn = 20; % number of elements per cluster
        h =[];
        for i = 1:m
           h = [h;epsilon*rand(nn,1)+e(i)];
        end
        H = diag(h);
        n = size(H,2);
        [Q,~] = qr(rand(n,n));
        H = Q'*H*Q; % Unitary transform keeps eigenvalues intact
        % Construct low rank perturbation
        s2 = 2; 
        s = 2*s2; % rank of perturbation
        % High norm perturbation
        F = rand(n,s2)+1i*rand(n,s2);
        G = rand(n,s2)+1i*rand(n,s2);
        N = norm(F*G');
        F = F/N;       
    case 2
        % Example 6.1 with ||F*G||>||H||
        % Clusters of eigenvalues, variation on example 5.2 SUMR
        m = 10; % number of clusters
        R = -10; % right side interval
        L = 5; % left side interval
        e = L+(R-L)*rand(m,1);
        epsilon = 0.1; % spread of cluster
        nn = 20; % number of elements per cluster
        h =[];
        for i = 1:m
           h = [h;epsilon*rand(nn,1)+e(i)];
        end
        H = diag(h);
        n = size(H,2);
        [Q,~] = qr(rand(n,n));
        H = Q'*H*Q; % Unitary transform keeps eigenvalues intact
        % Construct low rank perturbation
        s2 = 2; 
        s = 2*s2; % rank of perturbation
        % High norm perturbation
        F = rand(n,s2)+1i*rand(n,s2);
        G = rand(n,s2)+1i*rand(n,s2); 
        N = norm(F*G');
        F = F/(0.1*N); 
    case 3
        % Example 6.2, example 3.2 article Embree et al.
        alpha = 0.125; % Parameters
        beta = 1;
        p = 6;
        m = 190;
        gamma = 4;
            % Matrix
        Lmin = (-beta:(beta-alpha)/p:-alpha);
        Lplus = (alpha:(beta-alpha)/m:beta);
        H = diag([Lmin,Lplus,0,0]);
        e = zeros(m+p+2+2,1);
        f1 = e;
        f2 = e;
        g1 = e;
        g2 = e;
        f1(end) = gamma*1i;
        f2(end-1) = -gamma*1i;
        g1(end) = 1;
        g2(end-1) = 1;
        F = [f1,f2];
        G = [g1,g2];
        n = size(H,2);
        s = 4;
    otherwise
        disp('Give different value')
        return   
end
%% Construct matrix A
A = H + F*G';
% Properties matrix A
condA = cond(A); % condition number
% Right hand side
b = rand(n,1);
maxit = n;
%% Plot eigenvalues
%====================================================
 eigplot(A);
 
%% Solve linear system Ax=b
%====================================================
% PGMRES Solve
tic
[x, res] = PGMRES(A,b,F,G,maxit,tol);
toc

% Implementation Beckermann PGMRES (fastgmres)
tic
[xtest, resfastgmres, V] = fastgmres(A,F,G,b,zeros(n,1),maxit,tol,1);
toc

% GMRES comparison unrestarted gmres
% % Implementation Matlab no export Arnoldi basis possible
% tic
% [xGMRES,~,~,~,resGMRES] = gmres(A,b,n-1,tol); 
% toc

% Implementation Sleijpen (export Arnoldi basis)
tic
[xGMRES,resGMRES,V2]=gmresSleijpen(A,b,zeros(n,1),tol,maxit-1);
toc

% Restarted GMRES comparison
restart = 2*s+6; % same storage amount as PGMRES
nrestart = n/(2*s+6);
tic
[xGMRESrestart,~,~,~,resGMRESrestart] = gmres(A,b,restart,tol,nrestart);
toc

% LMR comparison
tic
[xLMR, resLMR] = LMR(A,b,[],maxit-2,tol);
toc

err1 = norm(A*x-b)-res(end);
err2 = norm(A*xtest-b)-resfastgmres(end);
err3 = norm(A*xGMRES-b)-resGMRES(end);
err4 = norm(A*xLMR-b)-resLMR(end);
if err1 > tol || err2 > tol || err3 > tol
    display('Check calculated residuals')
    err1
    err2
    err3
end

%% Make plot of the residuals
%====================================================
t = length(res);
tt = length(resGMRES);
ttt = length(resLMR);
tttt = length(resfastgmres);
t5 = length(resGMRESrestart);
figure
semilogy(1:t, res, 'xb')
hold on
semilogy(1:tt, resGMRES, ':k',lw,2)
hold on
semilogy(1:ttt, resLMR, 'k',lw,2)
hold on
semilogy(1:tttt, resfastgmres, '--r',lw,2)
hold on
semilogy(1:t5, resGMRESrestart, '-.k',lw,2)
xlabel('MV');
ylabel('||r_m||_2');
title(['Convergence PGMRES vs GMRES and LMR with '...
   'n = ' num2str(n)]);
legend('PGMRES','GMRES','LMR','fastgmres',['GMRES(' num2str(restart) ')']);

%% Check orthogonality Krylov basis
%====================================================
orth = 0; % if 0 this part is not executed
if orth == 1
    [~,q] = size(V); 
    [~,q2] = size(V2); 
    % Compute orthogonality Arnoldi vectors
    O1 = zeros(q,1);
    O2 = zeros(q2,1);
    for k = 1:q
        O1(k)=norm(speye(k,k)-V(:,1:k)'*V(:,1:k));    
    end
    for k = 1:q2
        O2(k)=norm(speye(k,k)-V2(:,1:k)'*V2(:,1:k)); 
    end
    figure
    semilogy(1:q,O1,'k')
    hold on
    semilogy(1:q2,O2,'xk')
    title('Orthogonality Arnoldi vectors')
    xlabel('MV');
    ylabel('||I_k-V_k^*V_k||_2');
    legend('PGMRES','GMRES');

    % Compute partial orthogonality Arnoldi vectors
    % orthonormality between the last m Arnoldi vectors
    % for R<=m<=L
    L = 5;
    PO1 = PartOrth(V,L,'PGMRES');
    PO2 = PartOrth(V2,L, 'GMRES'); 
end