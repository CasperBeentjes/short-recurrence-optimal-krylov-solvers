%% Test suite for SUMR
% For further description of examples see 
% http://people.maths.ox.ac.uk/beentjes/Essays/
clear all; close all
addpath('../Solvers/') % load solvers
lw = 'linewidth';

%% Examples
% All involving matrices of the form A = shift*I + scaling*U, with U
% unitary
shift = 1.1;
scaling = 1; 
tol = 1e-14; % Tolerance
n = 200; % Matrix size
maxit = 1.05*n; % Number of allowed iterations
M = input('Enter example number (1,2,3): ');
%====================================================
switch M
    case 1
        % Example 5.1
        [W,~] = qr(rand(n,n));
        Lambda = diag(exp(1i*(-pi/4+ (pi/2).*rand(n,1))));
        for k = 1:6
           Lambda(k,k) = exp(pi*1i*(k-1)/6);
        end
        U = W*Lambda*W';
    case 2
        % Example 5.2
        [U,~] = qr(rand(n,n)); % real
        [U,~] = qr(rand(n,n)+1i*rand(n,n)); % complex
    case 3
        % Example 5.3
        m = 50; % number of clusters
        e = 2*pi*1i*rand(m,1);
        epsilon = 0.01; % spread of cluster
        nn = 20; % number of elements per cluster
        UU =[];
        for i = 1:m
           UU = [UU;exp(2*pi*1i*epsilon*rand(nn,1)+e(i))];
        end
        UU = diag(UU);
        [n,~]=size(UU);
        [W,~] = qr(rand(n,n));
        U = W*UU*W';
    case 4
        % Example 5.4
        [W,~] = qr(rand(n,n));
        lambda = exp(1i*(-pi/4+ (pi/2).*rand(n/2,1)));
        lambda = [lambda; exp(1i*(7*pi/6+ (pi/6).*rand(n/2,1)))];
        Lambda = diag(lambda);
        for k = 1:6
           Lambda(k,k) = exp(pi*1i*(k-1)/6);
        end
        U = W*Lambda*W';
    otherwise
        disp('Give different value')
        return
end
%% Construct shifted Unitary matrix
% Check unitarity matrix U
Depunit = norm(U*U'-eye(n));
if Depunit > 1e-6
    display('Non unitary matrix')
    Depunit
end

A = scaling*U+shift*eye(n);
% Exact solution
% X = ones(n,1);
% b = A*X;
b = rand(n,1); % As done by Jagels & Reichel

%% Plot eigenvalues
%====================================================
 eigplot(A);

%% Solve linear system Ax=b
%====================================================
% SUMR 
tic
[xSUMRnonnorm,resSUMRnonnorm, V] = SUMRnonnormalized(U, b, shift, scaling, maxit, tol, [], 1);
toc

% SUMR with explicit normalisation
tic
[xSUMR,resSUMR, V2] = SUMR(U, b, shift, scaling, maxit, tol, [], 1);
toc

% Restarted GMRES
tic
[xGMRESrestart,~,~,~,resGMRESrestart] = gmres(A,b,6,tol,n/6);
toc

% GMRES comparison unrestarted gmres
% set restart to n+1 to force no restart
tic
[xGMRES,~,~,~,resGMRES] = gmres(A,b,n+1,tol);
toc

% LMR comparison
tic
[xLMR, resLMR] = LMR(A,b,[],maxit-2,tol);
toc

% Error in norm calculation
err = norm(A*xSUMR-b)-resSUMR(end);
err2 = norm(A*xSUMRnonnorm-b)-resSUMRnonnorm(end);
err3 = norm(A*xGMRES-b)-resGMRES(end);
if err > 10*tol || err2 > 1e-6
    display('Calculated residual SUMR not correct')
    err
    err2
end
        
%% Check orthogonality Krylov basis
%====================================================
[~,q] = size(V);
[~,q2] = size(V2);
orth = 0; % if 0 this part is not executed
if orth == 1
    % Compute orthogonality Arnoldi vectors
    O1 = zeros(q,1);
    O2 = zeros(q2,1);
    for k = 1:q
        O1(k)=norm(speye(k,k)-V(:,1:k)'*V(:,1:k));    
    end
    for k = 1:q2
        O2(k)=norm(speye(k,k)-V2(:,1:k)'*V2(:,1:k)); 
    end
    figure
    semilogy(1:q,O1,'k',lw,2)
    hold on
    semilogy(1:q2,O2,'xk',lw,2)
    title('Orthogonality Arnoldi vectors')
    xlabel('MV');
    ylabel('||I_k-V_k^*V_k||_2');
    legend('SUMR', 'SUMR (explicit normalization)');
    % legend('GMRES', 'SUMR (explicit normalization)');

    % Compute partial orthogonality Arnoldi vectors
    % orthonormality between the last m Arnoldi vectors
    % for R<=m<=L
    L = 5;
    PO1 = PartOrth(V,L,'SUMR');
    % PO1 = PartOrth(V,L,'GMRES');
    PO2 = PartOrth(V2,L, 'SUMR (explicit normalization)'); 
end


%% Check normality of complete Krylov basisvectors
%====================================================
nv = 0; % if 0 this part is not executed
if nv == 1
    NV1 = zeros(q,1);
    NV2 = zeros(q2,1);
    for k = 1:q
        NV1(k)=abs(1-norm(V(:,k)));
    end
    for k = 1:q2
        NV2(k)=abs(1-norm(V2(:,k)));
    end
    figure
    semilogy(1:q,NV1, 'k',lw,2)
    hold on
    semilogy(1:q2,NV2, 'xk',lw,2)
    xlabel('MV');
    xlim([0,30]);
    ylabel('|1-||v_k||_2|');
    legend('SUMR', 'SUMR (explicit normalization)','Location','best');
end

%% Make plot of the residuals
%====================================================
t = length(resSUMRnonnorm);
tt = length(resGMRES);
ttt = length(resLMR);
tttt = length(resSUMR);
figure
semilogy(1:tttt, resSUMR, '--k',lw,2)
hold on
semilogy(1:tt, resGMRES, 'xb',lw,2)
semilogy(1:ttt, resLMR, 'k',lw,2)
%semilogy(1:t, resSUMRnonnorm, '-.r',lw,2)
hold off
xlabel('MV');
xlim([0,maxit])
ylabel('||r_m||_2');
title(['Convergence SUMR vs GMRES and LMR with \zeta = '...
    num2str(shift) ', \rho = ' num2str(scaling) ' and n = ' num2str(n)]);
legend('SUMR (explicit normalization)','GMRES','LMR','SUMR');